import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Scanner;

public class Day04 {
    private static final ArrayList<String> inputs = new ArrayList<>();
    public static void main(String[] args) {
        generator();
        part01();
        part02();
    }
    private static void generator() {
        File inputFile = new File("./inputs/InputDay04/input.txt");
        try {
            Scanner in = new Scanner(inputFile);
            while (in.hasNextLine()) {
                String[] arr = in.nextLine().split(",");
                inputs.add(arr[0]);
                inputs.add(arr[1]);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void part02(){
        int sum = 0;
        System.out.printf("Any overlapping sets: %d %n", sum);
    }

    private static void part01(){
        int sum = 0;
        for(int i = 1; i < inputs.size(); i += 2){
            if(containedSet(inputs.get(i).split("-"), inputs.get(i-1).split("-"))){
                sum++;
            }
        }
        System.out.printf("Fully contained sets: %d %n", sum);
    }

    private static boolean containedSet(String[] input, String[] input1){
        int startRange1 = Integer.parseInt(input[0]);
        int endRange1 = Integer.parseInt(input[1]);
        int startRange2 = Integer.parseInt(input1[0]);
        int endRange2 = Integer.parseInt(input1[1]);
        if(startRange1 <= startRange2 && endRange1 >= endRange2){
            return true;
        } else return startRange2 <= startRange1 && endRange2 >= endRange1;
    }


}
