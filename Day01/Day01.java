import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day01 {

    public static void main(String[] args) {
        part01();
        part02();
    }
    private static void part01(){
        int highest = 0;
        int total = 0;
        ArrayList<Integer> values = new ArrayList<>();
        for(String string : generator()){
            if(!string.equals("")){
                values.add(Integer.parseInt(string));
            } else {
                // Reached the end of the list of numbers, sum them up
                for(int calories : values){
                    total += calories;
                }
                if(total > highest){
                    highest = total;
                }
                total = 0;
                values.clear();
            }
        }
        System.out.println(highest);
    }

    private static void part02(){
        ArrayList<Integer> sums = new ArrayList<>();
        int total = 0;
        ArrayList<Integer> values = new ArrayList<>();
        for(String string : generator()){
            if(!string.equals("")){
                values.add(Integer.parseInt(string));
            } else {
                for(int calories :values){
                    total += calories;
                }
                sums.add(total);
                total = 0;
                values.clear();
            }
        }
        // This is bad
        // I had to do this because there is no EOF normally unless I use a FileReader
        for(int remainder : values){
            total += remainder;
        }
        sums.add(total);
        sums.sort(Collections.reverseOrder());
        System.out.println(sums.get(0) + sums.get(1) + sums.get(2));
    }

    private static ArrayList<String> generator(){
        ArrayList<String> inputs = new ArrayList<>();
        File inputFile = new File("./inputs/InputDay01/Input.txt");
        try{
            Scanner in = new Scanner(inputFile);
            while(in.hasNextLine()){
                inputs.add(in.nextLine());
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return inputs;
    }
}
