import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day03 {
    private static final ArrayList<String> inputs = new ArrayList<>();
    private static final int forSmall = 96;
    private static final int forCaps = 38;

    public static void main(String[] args) {
        generator();
        part01();
        part02();
    }


    private static void generator() {
        File inputFile = new File("Day03/Inputs/Input.txt");
        try {
            Scanner in = new Scanner(inputFile);
            while (in.hasNextLine()) {
                inputs.add(in.nextLine());
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void part01() {
        int sum = 0;
        for (String input : inputs) {
            String firstHalf = splitString(input, true);
            String secondHalf = splitString(input, false);
            for (int j = 0; j < firstHalf.length(); j++) {
                if (compareCharInString(firstHalf.charAt(j), secondHalf)) {
                    sum += priorityLevel(firstHalf.charAt(j));
                    break;
                }
            }
        }
        System.out.println(sum);
    }

    private static void part02() {
        int sum = 0;
        for (int i = 0; i < inputs.size(); i += 3) {
            String one = inputs.get(i);
            String two = inputs.get(i + 1);
            String three = inputs.get(i + 2);
            for (int j = 0; j < one.length(); j++) {
                if (compareTwoStringsAndChar(one.charAt(j), two, three)) {
                    sum += priorityLevel(one.charAt(j));
                    break;
                }
            }
        }
        System.out.println(sum);
    }

    private static boolean compareTwoStringsAndChar(char c, String stringOne, String stringTwo) {
        for (int i = 0; i < stringOne.length(); i++) {
            if (c == stringOne.charAt(i)) {
                if (compareCharInString(stringOne.charAt(i), stringTwo)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean compareCharInString(char c, String str) {
        for (int i = 0; i < str.length(); i++) {
            if (c == str.charAt(i)) {
                return true;
            }
        }
        return false;
    }


    private static String splitString(String str, boolean firstHalf) {
        if (firstHalf) {
            return str.substring(0, (str.length() / 2));
        } else {
            return str.substring((str.length() / 2));
        }
    }


    private static int priorityLevel(char c) {
        if (c > 64 && c < 91) {
            return c - forCaps;
        } else {
            return c - forSmall;
        }
    }
}
