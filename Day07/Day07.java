import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.TreeSet;

public class Day07 {
    private static final ArrayList<String> inputs = new ArrayList<>();
    private static String path = "";
    private static String PWD = "";
    private static Node current = new Node();
    private static final Node root = new Node("/");
    private static final int maxSize = 100000;
    public static void main(String[] args) {
        generator();
        part01();
    }
    private static void generator() {
        File inputFile = new File("./inputs/InputDay07/input.txt");
        try {
            Scanner in = new Scanner(inputFile);
            while (in.hasNextLine()) {
                inputs.add(in.nextLine());
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void part01(){
        path = root.getValue();
        PWD = root.getValue();
        current = root;
        int totalSum = 0;
        /*
        Every time we cd we create a new node with that name
        ls creates and adds children
         */
        for(int i = 1; i <inputs.size(); i++){
            String[] command = inputs.get(i).split(" ");
            switch (command[1]){
                case "ls" -> {
                    ls(i + 1, current);
                    System.out.println(sumFileSize(current.getChildren()));
                    totalSum += sumFileSize(current.getChildren());
                }
                case "cd" -> {
                    cd(command, current);
                }
            }
        }
        System.out.println(totalSum);
    }

    private static void ls(int index, Node node){
        ArrayList<Node> nodes = new ArrayList<>();
        for(int i = index; i < inputs.size(); i++){
            if(inputs.get(i).charAt(0) == '$'){
                break;
            }
            nodes.add(new Node(inputs.get(i), node));
        }
        node.setChildren(nodes);
    }

    private static void cd(String[] input, Node node){
        switch (input[2]){
            case ".." -> {
                current = current.getParent();
            }
            case "/" -> {
                current.goBack(root);
            }
            default -> {
                current = new Node(input[2], node);
            }
        }
    }

    private static int sumFileSize(ArrayList<Node> files){
        int sum = 0;
        for (Node file : files) {
            String[] line = file.getValue().split(" ");
            if (!Objects.equals(line[0], "dir")) {
                System.out.println(Integer.parseInt(line[0]));
                sum += Integer.parseInt(line[0]);
            }
        }
        return sum;
    }
}

class Node{
    String value;
    Node parent = null;
    ArrayList<Node> children = new ArrayList<>();

    public Node(String value){
        this.value = value;
    }

    public Node(){}
    public Node(String value, ArrayList<Node> children){
        this(value);
        this.children = children;
    }

    public Node(String value, Node parent){
        this(value);
        this.parent = parent;
    }

    public Node getParent(){
        return parent;
    }

    public String getValue(){
        return value;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void setChildren(ArrayList<Node> children) {
        this.children = children;
    }

    public ArrayList<Node> getChildren() {
        return children;
    }

    public void addChild(Node node){
        children.add(node);
    }

    public boolean isChildOf(Node node){
        for(int i = 0; i < node.getChildren().size(); i++){
            if(Objects.equals(node.getChildren().get(i).getValue(), this.getValue())){
                return true;
            }
        }
        return false;
    }

    public Node goBack(String value){
        if(Objects.equals(this.getValue(), value)){
            return this;
        } else {
            return goBack(this.getParent().getValue());
        }
    }

    public Node goBack(Node node){

        boolean equalAndNotChild = Objects.equals(node.getValue(), this.getValue()) && !this.isChildOf(node);
        if(this.getParent() != null || equalAndNotChild){
            return this;
        } else {
            return goBack(node.getParent());
        }
    }
}
