import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day02 {
    private static ArrayList<String> inputs = new ArrayList<>();

    public static void main(String[] args) {
        generator();
        part01();
        part02();
    }

    private static void generator() {
        File inputFile = new File("inputs/InputDay02/Input.txt");
        try {
            Scanner in = new Scanner(inputFile);
            while (in.hasNextLine()) {
                inputs.add(in.nextLine());
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


    /*
     A B C = Opponent, values: 65, 66, 67
     X Y Z = Us, values: 88, 89, 90
     if difference between the choices is 23, then it is a draw
     Left is player, right is opponent
     Winning cases:
     (Paper vs Rock = 89 - 65 = 24
     (Scissors vs Paper) = 90 - 66 = 24
     (Rock vs scissors) = 88 - 67 = 21
     Losing cases:
     (rock vs paper) = 88 - 66 = 22
     (paper vs scissors) = 89 - 67 = 22
     (scissors vs rock) = 90 - 65 = 25
     */
    private static void part01() {
        int totalScore = 0;
        for (String input : inputs) {
            char opponent = input.charAt(0);
            char player = input.charAt(2);
            int playerChoiceScore = player - 87;
            int verdict = player - opponent;
            if (verdict == 23) {
                totalScore += draw() + playerChoiceScore;
            }
            if (verdict == 24 || verdict == 21) {
                totalScore += win() + playerChoiceScore;
            }
            if (verdict == 22 || verdict == 25) {
                totalScore += loss() + playerChoiceScore;
            }
        }
        System.out.printf("Part 1 total score: %d %n", totalScore);
    }

    /*
     X means lose
     Y means draw
     Z means win
     A B C = Opponent, values: 65, 66, 67
     X Y Z = Us, values: 88, 89, 90
    */
    private static void part02() {
        int totalScore = 0;
        for (String input : inputs) {
            char opponent = input.charAt(0);
            char player = input.charAt(2);
            int opponentChoiceScore = opponent - 64;
            switch (player) {
                case 'Y' -> totalScore += opponentChoiceScore + draw();
                case 'X' -> {
                    if (opponent != 'A') {
                        totalScore += opponentChoiceScore - 1 + loss();
                    } else {
                        totalScore += opponentChoiceScore + 2 + loss();
                    }
                }
                case 'Z' -> {
                    if (opponent != 'C') {
                        totalScore += opponentChoiceScore + 1 + win();
                    } else {
                        totalScore += opponentChoiceScore - 2 + win();
                    }
                }
            }
        }
        System.out.printf("Part 2 total score: %d", totalScore);
    }

    private static int win() {
        return 6;
    }

    private static int draw() {
        return 3;
    }

    private static int loss() {
        /*
         _______________
         |      |   | i
         _______|_______
         ||     |   | __
         _______|_______
         */
        return 0;
    }

}
